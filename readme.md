# Semestral Work Distributed Systems & Computing - TImothée Tailleur

FIT username: tailltim

## Project Overview

Implementation of Hirshberg-Sinclair algorithm for leader election, on a 5-node topology.
Implementation of a simple chat that allows nodes to communicate with one another using logical time to synchronize messages

## Classes Organization

# App package

NodeInterface defines the rmi methods for running Hi-Si algorithm as well as the rmi methods for the chat implementation. It has methods for topology control, creation & change (connect,disconnect,reportCrash), methods for 
leader election (send, receive, getters & setters for right & left neighbor), methods for the chat function (send chat message, forward chat message)

Node is the actual implementation: Each node is identified by IP+port, has ID of its left & right node and remote stubs for the rmiregistry, its left & right neighbor


# Log package

Loggable defines an interface with a single method that returns a String to identify a Node in logs (Hostname/IP:Port)

Logger is a class that implements a simple logger that logs infos, debug and error messages with physical & logical time.

# Model package

Direction is a simple enum that has values: {LEFT,RIGHT,BOTH} that will be used to send probes the right way during the Hi-Si algorithm

MessageType is another enum to define types of probes during the Hi-Si algorithm: Election (when a node sends his id during election phase), Announcement (when a node announces itself as winner of the election), Reply (when a node replies to an election probe)

Probe: The actual model for a probe during Hi-Si algoritm ; has a unique id, id of the originating node and number of hops and number of current phase of the algorithm

ChatMessage: A simple chat message with ID of originating & receiving node, logical clock, the actual message and physical time that will be set by the leader

# View Classes

NodeGUI: GUI for a single node. Has a window for logging in and a window with a list of connected nodes & messages exchanged with each of them. Each process runs a Thread to update the list every 5 of connected nodes & messages every 5 seconds.

ChatArea: The area of the GUI that displays messages.

