package model;

import java.io.Serializable;
import java.net.InetSocketAddress;
import java.util.Calendar;
import java.util.Random;

public class Probe implements Serializable{
	
    
   public InetSocketAddress src_id; //IP @s for the source node for this probe
   public InetSocketAddress last_id; //IP @s of the last node to receive the probe 
   public long id;						//Unique identifier for probe
   public int phase = 0;				//Phase in the algorithl (used to determine max number of hops
   public int hops = 0;					//Current number of hops
   
   public LamportClock clock;
   
   private Random rand = new Random();
   
   public MessageType type;
   public Direction direction;

   //When the probe is created, source = last and the phase is entered as parameter, id is set randomly
   public Probe( InetSocketAddress nodeID, MessageType msgtype, int phase, LamportClock clk ) {
	   src_id = nodeID;
	   last_id = nodeID;
       type = msgtype;
       clock=clk;
       
       this.phase = phase;
       
       rand.setSeed( Calendar.getInstance().getTimeInMillis() );
       id = rand.nextLong();
   }

}
