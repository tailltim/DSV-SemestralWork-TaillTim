package model;

import java.io.Serializable;
import java.net.InetSocketAddress;
import java.util.Date;

public class ChatMessage implements Serializable{
	
	public String msg;
	
	public InetSocketAddress from;
	
	public InetSocketAddress to;
	
	public Date time;
	
	public LamportClock logicalClock;
	
	public boolean withTimeStamp=false; //Will be true when the leader has seen this message and applied a timestamp
	
	public boolean destinationReached = false; //Will be true when the destination node has been reached after getting a timestamp
	
	public boolean originReached = false; //Will be true when the origin has been reached after getting a timestamp
	
	public boolean propagated = false; // destinationReached&&originReached ==> Both nodes have the message with timestamp ; message should be killed
	
	public ChatMessage(String message, InetSocketAddress sender, InetSocketAddress receiver,LamportClock clock) {
		msg = message;
		from = sender;
		to = receiver;
		logicalClock = clock;
	}
	
	public void setTime(Date timeStamp)
	{
		time = timeStamp;
	}

}
