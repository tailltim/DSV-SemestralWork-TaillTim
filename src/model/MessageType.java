package model;

//Enum for types of messages sent during Hi-Si algorithm
public enum MessageType {
	ELECTION,	//During Election phase
	REPLY,		//Reply to an election probe
	ANNOUNCEMENT; //Annouce yourself as a leader

	@Override
	public String toString() {
		String ret ="";
		switch(this)
		{
		case ELECTION:
			ret="Election";
			break;
		case REPLY:
			ret="Reply";
			break;
        case ANNOUNCEMENT:
            ret = "Announcement";
            break;
		}
		return ret;
	}
}
