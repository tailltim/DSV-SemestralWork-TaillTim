package model;

//Enum that represents direction of messages for Hi-Si algorithm
public enum Direction {
	LEFT,
	RIGHT,
	BOTH;
	
	@Override
	public String toString() {
		String ret="";
		switch(this)
		{
		case LEFT:
			ret="Left";
			break;
		case BOTH:
			ret="Both";
			break;
		case RIGHT:
			ret="Right";
			break;
		}
		return ret;
	}

}
