package model;
import java.io.Serializable;

public class LamportClock implements Serializable{
	
	private Long counter;

	public boolean equals(Object o) {
	   if (o == null || getClass() != o.getClass())
	     return false;
	   return this.counter == ((LamportClock) o).counter;
	 }
	  
	 public String toString() {
	   return counter.toString();
	 }
	/**
	 * Compare clocks and set new value as max(current stamp, received stamp) + 1
	 * @param o
	 */
	public void compareClocks(LamportClock o) {
		counter = Long.max(counter, o.counter) +1;
	}
	
	public void incrementCounter()
	{
		counter++;
	}
	
	public Long getTimeStamp()
	{
		return counter;
	}
	
	public void setCounter(Long i)
	{
		counter = i;
	}
	
	public LamportClock() {
		this(0L);
	}
	
	public LamportClock(Long time)
	{
		counter = time;
	}
}
