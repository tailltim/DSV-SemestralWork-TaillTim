package view;

import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;


import app.Node;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Separator;

import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;

import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import model.ChatMessage;




public class NodeGUI extends Application {
	
	private static Node  node;
	
	private static String registryIP= new String();
	
	private static String connectToIp = new String();

	private static Separator separator = new Separator();
	
	private static ChatArea chatArea = new ChatArea();
	
	private static Button inputButton = new Button();
	
	private static Button refreshButton = new Button();
	
	private static TextField input = new TextField();
	
	private static ListView<String> list = new ListView<String>();
	
	private static InetSocketAddress receiver;
	
	private static Thread updateThread;
	
	private static Stage stage;
	
	  
    private Parent loginWindow() {
    
    VBox root = new VBox();
    
    HBox registryHBox = new HBox();
    
    HBox IPHBox= new HBox();
    
	Label regInputLabel = new Label("RMI Registry IP: ");
	TextField regIPInput = new TextField("10.0.1.101");
	registryHBox.getChildren().add(regInputLabel); registryHBox.getChildren().add(regIPInput);

	Label IPInputLabel = new Label("IP of the Node you want to connect to : ");
	TextField IPInput = new TextField("10.0.1.102");
	IPHBox.getChildren().add(IPInputLabel); IPHBox.getChildren().add(IPInput);
	
	Button btn = new Button();
		
	btn.setText("Login");
	
	btn.setOnAction(new EventHandler<ActionEvent>() {
    	 
        @Override
        public void handle(ActionEvent event) {
        	registryIP = regIPInput.getText();
        	connectToIp=  IPInput.getText();
			try {
				node.login(new InetSocketAddress(connectToIp, 10), new InetSocketAddress(registryIP,1099));
				changeScene(2);
			} catch (RemoteException e) {
				System.out.println("Failed to login Node - Check registry IP, node IP");
				e.printStackTrace();
			}
        }
	});
	
	root.getChildren().add(registryHBox); root.getChildren().add(IPHBox); root.getChildren().add(btn);
	
	return root;
	} 

	public Parent chatWindow()
	{
        
        HBox root = new HBox();
        
        update();
        root.getChildren().addAll(list);

        
        root.getChildren().add(separator);
        
        VBox messagesAndInput = new VBox();
        
        messagesAndInput.getChildren().add(chatArea);
        
        inputButton.setText("Enter");
        inputButton.setOnAction(new EventHandler<ActionEvent>() {
        	 
            @Override
            public void handle(ActionEvent event) {
            	String text = input.getText();
            	ChatMessage message = new ChatMessage(text, node.nodeID, receiver,node.loggingClock);
            	try {
					node.getLeft().message(message);
					update();
				} catch (RemoteException e) {
					e.printStackTrace();
				}
            }
        });

        messagesAndInput.getChildren().add(new HBox(input,inputButton));
        
        root.getChildren().add(messagesAndInput);
        
        return root;
	}
	
	public static synchronized void update()
	{
		if (node.nodes != null)
		{
			ObservableList<String> items = FXCollections.observableArrayList(node.nodes);
			list.setItems(items);
			list.setOnMousePressed(new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent event) {
					String host = list.getSelectionModel().getSelectedItem();
					Integer port = 10;
					InetSocketAddress address = new InetSocketAddress(host, port);
					receiver = address;
					chatArea.updateMessages(node.chatMessages.get(address));
				}
			});
			
			chatArea.updateMessages(node.chatMessages.get(receiver));
		}	
	}
	
	public void logout(WindowEvent e)
	{
		e.consume();
		try {
			node.logout();
		} catch (NotBoundException e1) {
			System.out.println("Node was never bound");
			e1.printStackTrace();
		}
		changeScene(1);
	}
	
	public void quit(WindowEvent e)
	{
		Platform.exit();
		updateThread.interrupt();
	}
	
	public void changeScene(int nb)
	{
		if (nb==1)
		{
			stage.setScene(new Scene(loginWindow(),400,200));
			stage.setOnCloseRequest(e -> quit(e));
		}
		if (nb==2)
		{
			stage.setScene(new Scene(chatWindow(),800,600));
			stage.setOnCloseRequest(e -> logout(e));
		}
	}
	
    @Override
    public void start(Stage primaryStage) {    
    	
        stage = primaryStage;

        primaryStage.setTitle("DSV Semestral Work");
        changeScene(1);
        primaryStage.show(); 

    }
    
  
	public static void main(String[] args)  {
	 	
	 	try {
			node = new Node();
		} catch (RemoteException | UnknownHostException e) {
			System.out.println("Failed to create Node - Check stacktrace");
			e.printStackTrace();
		}
	 	
	 	 Task task = new Task<Void>() {
	         @Override
	         public Void call() throws Exception {
	        	 while (true)
	        	 {
	        	   Platform.runLater ( () -> update());
	        	   Thread.sleep (5000);
	        	 }
             }
         };

         updateThread = new Thread(task);
         
         updateThread.start();

        launch(args);
    }
}
