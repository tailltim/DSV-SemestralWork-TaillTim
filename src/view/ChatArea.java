package view;

import java.net.InetSocketAddress;
import java.util.ArrayList;


import javafx.scene.control.TextArea;

import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import model.ChatMessage;

public class ChatArea extends Pane {
	
	private  TextArea messages = new TextArea();
	
	public ChatArea()
	{
		super();

		
		messages.setEditable(false);
		messages.setPrefHeight(550);
		
        VBox root = new VBox(messages);
        
		this.getChildren().add(root);
	}
	
	public void updateMessages(ArrayList<ChatMessage> list)
	{
		if (list !=null)
		{
			String text = createText(list);
			messages.setText(text);
		}

	}
	
	private String createText(ArrayList<ChatMessage> list)
	{
		String txt = new String();
		for (ChatMessage message : list)
		{
			txt+=
			String.format(
					"%1$s [%2$tH:%2$tM:%2$tS:%2$tL] : %3$s ",message.from.toString(),message.time,message.msg)+" \n";
		}
		return txt;
	}

}
