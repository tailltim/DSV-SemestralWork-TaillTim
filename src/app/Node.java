package app;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.rmi.AccessException;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.ServerNotActiveException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;
import java.lang.Thread.State;

import log.Loggable;
import log.Logger;
import model.ChatMessage;
import model.Direction;
import model.LamportClock;
import model.MessageType;
import model.Probe;


public class Node extends UnicastRemoteObject implements NodeInterface, Loggable, Runnable {
	//Attributes

	public Node() throws RemoteException, UnknownHostException {
		super();
		Random rand = new Random(); rand.setSeed(Calendar.getInstance().getTimeInMillis());
		logger = new Logger("node"+rand.nextInt()+".log");
		nodeID = new InetSocketAddress(InetAddress.getLocalHost(), 10);
		messages = new LinkedList<Probe>();
		leaderElectionThread = new Thread(this);
		loggingClock = new LamportClock();
		chatMessages = new HashMap<InetSocketAddress,ArrayList<ChatMessage>>();		
		nodes = new ArrayList<String>();
		logger.info(String.format("Created Node %s", nodeID.getAddress().getHostAddress()), this, loggingClock);
	}

	public InetSocketAddress nodeID; //Node Unique ID (IP @ + port)
	
	public InetSocketAddress currentLeaderID; //Unique ID for currentLeader
	
	public InetSocketAddress leftNodeID; // Required in case a node fails (no access to id() remote getter)
	
	public InetSocketAddress rightNodeID;	// Required in case a node fails (no access to id() remote getter)
	
	private boolean participating=false; //True when the node is participating in the election process
	
	private boolean isLeader=false; //True when a node is the leader
	 
	private boolean temporalLeader = false; //True when the node has announced itself as leader
	
	private boolean hasLeader=false; //True when the node has a leader
	
	private boolean activated = false; //True when the node has received an election probe and is activated

	private boolean registryAccessible = false; //
	
	private int phase=0; //Current phase of the algorithm
	private int replies = 0; //Number of replies received to an election probe
	
	public NodeInterface leftNode; //Left neighbor
	
	public NodeInterface rightNode; //Right neighbor

	public Registry reg;	//RMIRegistry Stub
	
    private Queue<Probe> messages; // Probe messages Queue
    
    private Logger logger; // Logger Object to log progress
    
    private Thread leaderElectionThread; // Hi-Si leader election algorithm thread
    
    public LamportClock loggingClock; //Logical time for logging
    
    public ArrayList<String> nodes; //ArrayList of nodes to be used in chat 
    
    public HashMap<InetSocketAddress,ArrayList<ChatMessage>> chatMessages; //Chat Messages
    

  //Topology creation, change & correction methods
    

	/**
	 * Fetch registry (or create it), set default neighbor values if 1st node ; connect to the ring otherwise
	 * @param connectToIP
	 * @param registryIP
	 * @throws AccessException 
	 * @throws RemoteException
	 */
	public void login(InetSocketAddress connectToIP, InetSocketAddress registryIP) throws AccessException, RemoteException
	{
		//Get the registry
		try {
			reg = LocateRegistry.getRegistry(registryIP.getAddress().getHostAddress(), registryIP.getPort());
		} 
		
		//If the registry is not available, that means we are the first node
		catch (RemoteException | NullPointerException e  ) {
			logger.error("Couldn't get reference to registry - check IP ? - Creating Local registry on port 1099",this,loggingClock);
			e.printStackTrace();
			try {
				reg=LocateRegistry.createRegistry(1099);
			} catch (RemoteException e1) {
				logger.error("Could not create registry",this,loggingClock);
				e1.printStackTrace();
			}
		}
		
		//If the node is on the same host as the rmi registry, it will handle binding of further node
		if (registryIP.getAddress().equals(nodeID.getAddress()))
			registryAccessible=true;
		
		//Use the registry to fetch reference on the node we want to connect to & launch connect on that host

		if (reg.list().length==0){
			logger.info("I am the first Node - setting my neighbour as me(left), me(right)", this, loggingClock);
			rightNode = this;
			leftNode = this;
			reg.rebind(nodeID.getAddress().getHostAddress(), (NodeInterface) this);
			return;
			}
		
		if (reg.list().length>=1){
			try {
				NodeInterface connectingNode = (NodeInterface) reg.lookup(connectToIP.getAddress().getHostAddress());
				
				
				connectingNode.connect(this);
				logger.info("Just used connect on the node I wanted to connect to !", this, loggingClock);
				System.out.println("My left: "+leftNode.id().toString() + "my right: "+rightNode.id().toString());
			
			} catch (NotBoundException e) {
				logger.error("IP not bound, check IP of node you want to connect to"+e,this,loggingClock);
			} catch (RemoteException e) {
				logger.error("RemoteException during connect call on leftNode" +e, this, loggingClock);
			}
		}

		//If 5 nodes have been registered, we start the Hi-Si algorithm
		if (reg.list().length==5){
			logger.info("Starting Hi-Si algorithm", this, loggingClock);
			rightNode.activate();
		}		
	}
	
	/**
	 * Clean logout
	 * @throws NotBoundException 
	 */
	public void logout() throws NotBoundException
	{
		try {
			leftNode.disconnect(this);
			if (leaderElectionThread.isAlive())
				leaderElectionThread.interrupt();
		} catch (RemoteException e) {
			// logger.error("Failed to disconnect using Left Node...Trying Right Node"+e,this,clock);
			e.printStackTrace();
			try {
				rightNode.disconnect(this);
			} catch (RemoteException e1) {
				//logger.error("Can't reach left or right node"+e,this,loggingClock);
				e1.printStackTrace();
			}
		}
	}
	
/**
 * In case a node is unreachable send the message the other way around with ID of failing node & discovery node
 * => Remote method reportCrash will be called on the available neighbor of current node and forward until the other neighbor of the failing node has been reached
 * @param failingProbe
 * @param discoveryNode
 * @param failingNodeID
 * @throws RemoteException 
 */
	public void unreachableNode(Object failingMessage, NodeInterface discoveryNode, InetSocketAddress failingNodeID) throws RemoteException
	{
		String messageType = failingMessage.getClass().getSimpleName();
		if (messageType == "Probe")
		{
			if (failingNodeID.equals(leftNodeID))
				leftNode = null; rightNode.reportCrash( this, failingNodeID,false,Direction.RIGHT,1);
			if (failingNodeID.equals(rightNodeID))
				rightNode = null ;leftNode.reportCrash( this, failingNodeID,false,Direction.LEFT,1);
		}
		
		if (messageType=="ChatMessage")
		{

			if (failingNodeID==currentLeaderID)
			{
				if (failingNodeID.equals(leftNodeID))
					leftNode = null;rightNode.reportCrash(this, failingNodeID,true,Direction.RIGHT,1);
				if (failingNodeID.equals(rightNodeID))
					rightNode = null ;leftNode.reportCrash(this, failingNodeID,true,Direction.LEFT,1);
			}else
			{
				if (failingNodeID.equals(leftNodeID))
					leftNode = null; rightNode.reportCrash(this, failingNodeID,false,Direction.RIGHT,1);
				if (failingNodeID.equals(rightNodeID))
					rightNode = null ; leftNode.reportCrash(this, failingNodeID,false,Direction.LEFT,1);
			}
		}
	}

	

  /**
   * On connection, set new Node as left and my old right neighbor as new node's right neighbor
   */
  public void connect(NodeInterface node) throws RemoteException{
  	NodeInterface oldRightNode = rightNode;
  	node.setLeft(this);
  	node.setRight(oldRightNode);
  	
  	if (registryAccessible)
  	{
  		reg.rebind(node.id().getAddress().getHostAddress(),node);
  	}else
  	{
  		leftNode.forwardBind(node);
  	}
  }

  /**
   * Used by a node for a clean logout
 * @throws NotBoundException 
   */
  public void disconnect(NodeInterface node) throws RemoteException, NotBoundException {
	if (leftNode != null && rightNode != null)
	{
	  	node.getLeft().setRight(node.getRight());
	  	node.getRight().setLeft(node.getLeft());
	}

  	if (registryAccessible)
  	{
  		reg.unbind(node.id().getAddress().getHostAddress());
  	}else
  	{
  		leftNode.forwardUnbind(node);
  	}
  }
 
  /**
   * Received when a node has discovered a failure in the network ; update neighbors if necessary ; forward otherwise
   * @param failingMessage
   * @param discoveryNode
   * @param failingNodeID
   * @param failingNodeIsLeader
   * @param forwardDirection
   * @throws RemoteException
   */
  public void reportCrash(NodeInterface discoveryNode, InetSocketAddress failingNodeID, boolean failingNodeIsLeader,Direction forwardDirection,int hops) throws RemoteException {
	  if (hops<4)
	  {
			if (failingNodeIsLeader)
		  		currentLeaderID = discoveryNode.id();
		  	
		  	if (leftNodeID.equals(failingNodeID))
		  	{
		  		setLeft(discoveryNode);
		  		return;
		  	}

		  	if (rightNodeID.equals(failingNodeID))
		  	{
		  		setRight(discoveryNode);
		  		return;
		  	}
		  	
		  	int newHops = hops+1;
		  	
		  	switch(forwardDirection)
		  	{
		  	case LEFT:
		  		leftNode.reportCrash(discoveryNode, failingNodeID, failingNodeIsLeader, forwardDirection,newHops);
		  		break;
		  	case RIGHT:
		  		rightNode.reportCrash(discoveryNode, failingNodeID, failingNodeIsLeader, forwardDirection,newHops);
		  		break;
		  	default:
		  		break;
		  	}
	  }
  
  }

  public void forwardBind(NodeInterface node)throws RemoteException
  {
	  if (registryAccessible){
		  reg.rebind(node.id().getAddress().getHostAddress(), node);
	  }else{
		  leftNode.forwardBind(node);
	  }
  }
	public void forwardUnbind(NodeInterface node)throws RemoteException
	{
		  if (registryAccessible){
			  try {
				reg.unbind(node.id().getAddress().getHostAddress());
			} catch (NotBoundException e) {
				logger.error("Failed to unbind Node - Node was never bound" +e, this, loggingClock);
			}
		  }else{
			  leftNode.forwardUnbind(node);
		  }
	}
	
    
   public boolean isLeader() {return isLeader;}
    
   public boolean hasLeader() {return hasLeader;}
	
	@Override
	public void probe(MessageType type, Direction direction) throws RemoteException {
    	loggingClock.incrementCounter();
        Probe probe = new Probe( nodeID, type, phase,loggingClock);
        
        logger.debug( String.format( "I'm about to probe(%d)", probe.id ), this, loggingClock);
        
        if( direction == Direction.BOTH || direction == Direction.LEFT ) {
        
            probe.direction = Direction.LEFT;
            send( leftNode, probe );
        }
        
        if( direction == Direction.BOTH || direction == Direction.RIGHT ) {
        
            probe.direction = Direction.RIGHT;
            send( rightNode, probe );
        }	
	}
	
	/**
	 * Set node as left Neighbor
	 * If node is different than null, doesn't have a right neighbor or its right neighbor
	 * isn't the current object then we set the right neighbor as the caller object (this)
	 */
	public void setLeft(NodeInterface node) throws RemoteException {
    	loggingClock.incrementCounter();
		leftNode=node;

		if ( (node!=null) &&(node.getRight()==null||!node.getRight().id().equals(nodeID)))
		{
			leftNodeID= node.id();
			node.setRight((NodeInterface) this);
		}
	}
	
	/**
	 * Set node as right Neighbor
	 * If node is different than null, doesn't have a left neighbor or its left neighbor
	 * isn't the current object then we set the left neighbor as the caller object (this)
	 */
	public void setRight(NodeInterface node) throws RemoteException {
    	loggingClock.incrementCounter();
		rightNode=node;

		System.out.println("Setting right Node");
		if ( (node!=null) &&(node.getLeft()==null||!node.getLeft().id().equals(nodeID)) )
		{
			rightNodeID = node.id();
			node.setLeft((NodeInterface) this);
		}

	}
	
	
	/**
	 * RMI Getter for left neighbor
	 */
	public NodeInterface getLeft() throws RemoteException {return leftNode;}
	
	/**
	 * RMI Getter for right neighbor
	 */
	public NodeInterface getRight() throws RemoteException {return rightNode;}
	
	/**
	 * RMI method used when a node receives a probe during Hi-Si algorithm
	 */
	public void send(Probe probe) throws RemoteException {
    	loggingClock.incrementCounter();
		logger.debug( String.format( "Just got a probe(%d) of type: %s", 
                probe.id, probe.type ), this, loggingClock);
		
		if( leaderElectionThread.getState() == State.NEW )
            activate();
        messages.add( probe );
	}
/**
 * Used to process probes based on their arrival in the Queue
 */
	public Probe receive() throws RemoteException {
    	loggingClock.incrementCounter();
		return messages.poll();
	}
	
	/**
	 * RMI Getter for node ID
	 */

	public InetSocketAddress id() throws RemoteException {
		return nodeID;
	}
	
	//Loggable Method
	@Override
	public String logIdent() {
		return "Node @"+nodeID.toString();
	}
	

	//General purpose methods
    /**
     * Send probe to node
     * @param node
     * @param probe
     * @throws RemoteException
     */
	public void send(NodeInterface node, Probe probe) throws RemoteException {
    	loggingClock.incrementCounter();
        node.send( probe );
	}
	
    /**
     * Reply to a probe
     * @param probe
     * @throws RemoteException
     */
    private  void reply( Probe probe ) throws RemoteException {
    	loggingClock.incrementCounter();
        probe.type = MessageType.REPLY;
                        
        if( probe.direction == Direction.RIGHT ) {
            probe.direction = Direction.LEFT;
            send( leftNode, probe );
        } else {
            probe.direction = Direction.RIGHT;
            send( rightNode, probe );
        }
    }
	
	/**
	 * Forward probe passed as parameter to neighbour
	 * @param probe
	 * @throws RemoteException
	 */
    private  void forward( Probe probe ) throws RemoteException {
    	loggingClock.incrementCounter();
    	probe.clock = loggingClock;
        if( probe.direction == Direction.RIGHT )
            send( rightNode, probe );
        else 
            send( leftNode, probe );
    }
    
	/**
	 * Process received Probe
	 * @param probe
	 * @throws RemoteException
	 */
    
    private  void processProbe( Probe probe ) 
            throws RemoteException {
    	loggingClock.compareClocks(probe.clock);
        
        logger.debug( String.format( "I am processing probe(%d) "
                + "now", probe.id ), this, loggingClock);
        
        probe.last_id = nodeID;
        
        probe.hops++;
        
        switch( probe.type ) {
            case ELECTION:
                logger.debug( String.format( "It's an Election probe "
                        + "originally from Node-%s", probe.src_id.getAddress().getHostAddress() ), this, loggingClock);
                if( probe.src_id.getAddress().hashCode() > nodeID.getAddress().hashCode() ) {
                    
                    int maxHops = (int)Math.pow( 2, probe.phase );
                    
                    if( probe.hops < maxHops ) {
                        forward( probe );

                    } else if( probe.hops == maxHops ) {

                        reply( probe );
                        
                    } 
                    
                } else if( probe.src_id.equals(nodeID) ) {
                    
                    if( hasLeader ) {
                        logger.debug( String.format( "I already have a "
                                + "leader: Node-%s", currentLeaderID.getAddress().getHostAddress() ), this, loggingClock);
                        break;
                    }
                    
                    isLeader = true;
                    hasLeader = true;
                    currentLeaderID = nodeID;
                    logger.debug( "Annoucing myself as winner", this, loggingClock);
                    
                    probe( MessageType.ANNOUNCEMENT, Direction.RIGHT );
                    
                } else logger.debug( String.format( "Swallowed probe(%d) "
                        + "because I'm a greater node than Node-%s", 
                        probe.id, probe.src_id.getAddress().getHostAddress() ), this, loggingClock);
            break;
                
            case REPLY:
                logger.debug( String.format( "It's a Reply probe for "
                        + "Node-%s", probe.src_id.getAddress().getHostAddress() ), this, loggingClock);
                if(!  probe.src_id.equals(nodeID) ) {
                    
                    forward( probe );
                   
                    
                } else {
                    replies++;
                    if( replies >= 2 ) {
                        phase++;
                        
                        logger.debug( "I am entering phase " + phase, this, loggingClock);
                        
                        probe( MessageType.ELECTION, Direction.BOTH );
                        
                        replies = 0;
                    }
                }
                
            break;
                
            case ANNOUNCEMENT:
                logger.debug( String.format( "It's an Announcement probe "
                        + "from Node-%s", probe.src_id.getAddress().getHostAddress() ), this, loggingClock);
                if( !hasLeader && (!probe.src_id.equals(nodeID))) {
                   
                    forward( probe );
                    
                    currentLeaderID = probe.src_id;
                    hasLeader = true;   
                } else temporalLeader = true;
                
            break;
        }
        
        logger.debug( String.format( "I finished processing probe(%d) "
                + "now", probe.id ), this, loggingClock);
        
    }
    
	/**
	 * Start hi-shi algoritm
	 * Set node to activated and create a new thread that will run the Hi-Si algorithm
	 * This method is called by the ring service after 5 nodes are successfully added
	 * and when a node receives an election probe for the first time
	 */
    public synchronized void activate() {
    	loggingClock.incrementCounter();
    	if( leaderElectionThread.getState() == State.NEW ) {
    		leaderElectionThread.start();
            activated = true;
            this.logger.debug( "I have been activated", this, loggingClock);
        }
    }
    
    /**
     * Run Hi-Si algorithm
     */
	@Override
	public void run() {
		 if( !participating ) {
	            try { 	
	            	loggingClock.incrementCounter();
	                probe( MessageType.ELECTION, Direction.BOTH );
	            } catch( RemoteException re ) {
	                logger.error( "The service failed: " + re, this, loggingClock);
	            }
	            participating = true;
	            logger.debug( "I am now participating", this, loggingClock);
	        }
	        
	        Probe p;
	        while( true ) {
	            
	           if( isLeader && temporalLeader )
	                break;
	           
	            try {
	                if( (p = receive()) != null ) {
	                	loggingClock.incrementCounter();
	                    processProbe( p );
	                    
	                    if( hasLeader && !isLeader )
	                        break;
	                }

	            } catch( RemoteException re ) {
	                logger.error( "The service failed in run(): " + re, this, loggingClock);
	                re.printStackTrace();
	            } 

	 
	        }

	        if( !isLeader )
	            logger.info( String.format( "Node-%s has chosen leader: "
	                    + "Node-%s", nodeID.getAddress().getHostAddress(), currentLeaderID.getAddress().getHostAddress() ), this, loggingClock);
	        else
	            logger.info( "I am leader", this, loggingClock);
	        
	    	logger.info("Hi-Si algorithm finished", this, loggingClock);
	    	
	    	try {
				finishHiSiAlgorithm();
			} catch (RemoteException e) {
				e.printStackTrace();
			}
	}
	
	public void finishHiSiAlgorithm() throws AccessException, RemoteException
	{
		nodes.addAll(Arrays.asList(reg.list()));
		nodes.remove(nodeID.getAddress().getHostAddress());
		
		for (String node : nodes)
		{
			chatMessages.put(new InetSocketAddress(node,10), new ArrayList<ChatMessage>());
		}
	}
	
	public void killLeaderElectionThread()
	{
		leaderElectionThread.interrupt();
	}
	
	//Chat Methods
	@Override
	public void message(ChatMessage msg)  {
		loggingClock.compareClocks(msg.logicalClock);
		
		//If the message has no timestamp, it is forwarded until the leader adds a timestamp
		if (!msg.withTimeStamp)
		{
			if (!isLeader)
			{
				try {
					forwardChatMessage(msg);
				} catch (RemoteException e) {
					logger.error("Remote error when trying to send a chat message "+e, this, loggingClock);
				}
				return;
			}else
			{
				msg.time = Calendar.getInstance().getTime();
				logger.info(String.format(
						"As leader, I just added a time stamp on a message from %s",msg.from.toString()),this, loggingClock);
				msg.withTimeStamp = true;
			}
		}
		
		/*If the message has a timestamp, it is forwarded until both origin and destination nodes have received the message with the timestamp
		then it is discarded
		*/
		if (msg.withTimeStamp)
		{
			if (msg.from.equals(nodeID))
			{
				chatMessages.get(msg.to).add(msg);
				msg.originReached=true;
			}
			if( msg.to.equals(nodeID))
			{
				chatMessages.get(msg.from).add(msg);
				msg.destinationReached=true;
			}
			
			msg.propagated=msg.destinationReached&&msg.originReached;
			
			if (!msg.propagated)
			{
				try {
					forwardChatMessage(msg);
				} catch (RemoteException e) {
					logger.error("Remote error when trying to send a chat message "+e, this, loggingClock);
				}
			}else
			{
				return;
			}
			
		}
	}

	public void forwardChatMessage(ChatMessage msg) throws RemoteException
	{
		rightNode.message(msg);
	}

}

