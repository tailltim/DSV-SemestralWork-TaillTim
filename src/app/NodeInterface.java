package app;
import java.net.InetSocketAddress;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;

import model.ChatMessage;
import model.Direction;
import model.MessageType;
import model.Probe;

public interface NodeInterface extends Remote {
	
	//Topology creation, correction & change methods
	
	public void connect(NodeInterface node) throws RemoteException; //Called to connect to the ring
	public void disconnect(NodeInterface node) throws RemoteException, NotBoundException; //Called to disconnect from the ring
	public void reportCrash(NodeInterface discoveryNode, InetSocketAddress failingNodeID,boolean failingNodeIsLeader, Direction forwardDirection,int hops) throws RemoteException;
	public void forwardBind(NodeInterface node)throws RemoteException; //Forward node to node with access to rmi registry
	public void forwardUnbind(NodeInterface node)throws RemoteException; //Forward node to node with access to rmi registry
	
	//Hi Si methods
	
	public void probe( MessageType type, Direction direction ) throws RemoteException; //Send type Message in Direction
    
   	public void setLeft( NodeInterface node ) throws RemoteException; //Set Left Node
   	public void setRight( NodeInterface node ) throws RemoteException; //Set Right Node

	public NodeInterface getLeft() throws RemoteException; //get Left Node
	public NodeInterface getRight() throws RemoteException; //get Right Node
	
	
	public void send( Probe probe ) throws RemoteException; //Will be called by other JVMs to send a message to our node
	public Probe receive() throws RemoteException;	//Will be called by the node to get Message from message queue
	
	public InetSocketAddress id() throws RemoteException; //Returns IP @ of node
	
	public void activate() throws RemoteException;
	
	 public boolean isLeader() throws RemoteException;
	
	//Chat methods
	public void message(ChatMessage msg) throws RemoteException;


}
