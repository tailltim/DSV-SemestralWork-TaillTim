package log;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import model.LamportClock;

public class Logger {
    
    private static BufferedWriter logFileWriter;
    
    private static String file;
    
    public Logger(String fileName)
    {
    	file=fileName;
    	setup();
    }
    
    public  void setup()
    {
    	try {
			logFileWriter = new BufferedWriter(new FileWriter(file));
			logFileWriter.write("Logging started at "+Calendar.getInstance().getTime()+ "\n");
			logFileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }

    public  void info( String msg, Loggable obj,LamportClock clock ) {
        String info = String.format(																														
            "[INFO][%1$tH:%1$tM:%1$tS:%1$tL][%2$s] - %3$s: %4$s%n", Calendar.getInstance().getTime(), clock.toString() ,(obj != null ) ? obj.logIdent():"Unknown", msg );
        
        write( info);
    }
    
    public  void warn( String msg, Loggable obj ,LamportClock clock) {
        String info = String.format(																														
            "[WARN][%1$tH:%1$tM:%1$tS:%1$tL][%2$s] - %3$s: %4$s%n", Calendar.getInstance().getTime(),clock.toString() ,(obj != null ) ? obj.logIdent():"Unknown", msg );
        
        write( info);
    }
    
    public  void debug( String msg, Loggable obj ,LamportClock clock) {
        String info = String.format(																														/* a small hack down below */
            "[DEBUG][%1$tH:%1$tM:%1$tS:%1$tL][%2$s] - %3$s: %4$s%n", Calendar.getInstance().getTime(), clock.toString(),(obj != null ) ? obj.logIdent():"Unknown", msg );
        
        write( info );
    }
    
    public  void error( String msg, Loggable obj ,LamportClock clock) {
        String info = String.format(																														/* a small hack down below */
            "[ERROR][%1$tH:%1$tM:%1$tS:%1$tL][%2$s] - %3$s: %4$s%n", Calendar.getInstance().getTime(),clock.toString() ,(obj != null ) ? obj.logIdent():"Unknown", msg );
        write( info );
    }

    
    private  void write( String msg ) {
    	try {
    		logFileWriter = new BufferedWriter(new FileWriter(file, true));
    		logFileWriter.append(msg);
    		logFileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    	
    	System.out.println(msg);
    }
}